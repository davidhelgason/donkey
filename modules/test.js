var watch = require("watch");
var fs = require('fs');
var async = require('async');
var he = require('./harvesteve.js');
var chokidar = require('chokidar');


console.log(he);

var pathToServiceConfiguration = "../../scfg.json";
var serviceConfiguration;

var thePath;
var watcher;

async.series([
    function (callback) {
        fs.readFile(pathToServiceConfiguration, 'utf8', function (err, data) {
            if (err) throw err;
            serviceConfiguration = JSON.parse(data);
            callback();
        });
    },
    function (callback) {
        thePath =serviceConfiguration.game.pathToLogs;
        watcher = chokidar.watch(thePath, {ignored: /^\./, persistent: true});
        watcher
            .on('add', file_add)
            .on('change', file_change)
            .on('unlink', file_unlink)
            .on('error', file_error);
    }
]);

function file_add(path) {
    console.log('File', path, 'has been added');
}
function file_change(path) {
    console.log('File', path, 'has been changed');
}
function file_unlink(path) {
    console.log('File', path, 'has been removed');
}
function file_error(error) {
    console.error('Error happened', error);
}

//Converter Class
var Converter = require("csvtojson").Converter;
var converter = new Converter({});

//end_parsed will be emitted once parsing finished
converter.on("end_parsed", function (jsonArray) {
    console.log(jsonArray); //here is your result jsonarray
});

var a_path = "C:\\Users\\david\\Documents\\EVE\\logs\\Marketlogs\\The Forge-Uranium Charge S-2015.10.23 220212.txt";

//read from file
require("fs").createReadStream(a_path).pipe(converter);