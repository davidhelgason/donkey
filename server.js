var http = require('http'),
    os = require('os'),
    express = require('express'),
    path = require('path'),
    async = require('async'),
    socketio = require('socket.io');

var port = 8080;

var router = express();
var server = http.createServer(router);
var io = socketio.listen(server);

router.use(express.static(path.resolve(__dirname, 'client')));

server.listen(port, process.env.IP || "0.0.0.0", function(){
    var addr = server.address();
    console.log("Chat server listening at", addr.address + ":" + addr.port);
});

